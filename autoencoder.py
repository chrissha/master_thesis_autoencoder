"""Simple autoencoder implementation. To use this create a class deriving from nn.Module and
supply it to the Agent class upon initiation.
"""
import os
import matplotlib.pyplot as plt
import torch
import torchvision
from torch import nn


class Agent():
    """Agent class to manage the autoencoder.

    Agent serves as an easy to use interface to the autoencoder. It features easy hyperparameter
    initialization, training, saving and loading parameters, and saving the results of a trained
    agent.
    """
    def __init__(self, autoencoder, epochs=10, batch_size=64, lr=0.001, lambd=0, loss='BCE',
                 verbose=10):
        """Initializes the agent that manages the autoencoder

        params:
            autoencoder (object): The autoencoder to use. The input to this network is defined by
                                  the dataset used in self.train(). Needs to derive from nn.Module
                                  class.
            epochs (int): How many epochs to train for
            batch_size (int): The batch size used for training
            lr (float): The learning rate used for training
            lambd (float): The L2 regularization parapemeter for more sparse weights.
            loss (string): The loss function to use for difference between input and the
                           reconstructed image. Can be 'BCE' or 'MSE'.
            verbose (int): 0 prints nothing. 10 prints every epoch. 20 prints every episode.
        """
        # Hyperparameters
        self.epochs = epochs
        self.batch_size = batch_size
        self.learning_rate = lr
        self.verbose = verbose

        self.device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

        # Initialize model
        self.model = autoencoder.to(self.device)
        self.model.device = self.device

        self.optimizer = torch.optim.Adam(self.model.parameters(), lr=self.learning_rate,
                                          weight_decay=lambd)
        self.loss = nn.BCELoss() if loss == 'BCE' else nn.MSELoss()


    def train(self, dataset):
        """Trains the autoencoder using a given dataset.

        params:
            dataset (torchvision.utils.data.Dataset): The dataset to train the autoencoder on.
        """
        dataloader = torch.utils.data.DataLoader(dataset, batch_size=self.batch_size, shuffle=True)

        for epoch in range(self.epochs):
            for i, batch in enumerate(dataloader):
                if isinstance(batch, list):
                    batch, _ = batch

                # Make 'prediciton' and calculate loss
                batch = batch.to(self.device)
                predictions = self.model(batch)
                loss = self.loss(predictions, batch)

                # Backprop
                self.optimizer.zero_grad()
                loss.backward()
                self.optimizer.step()

                if self.verbose >= 20:
                    print("Batch {}/{} complete".format(i+1, len(dataloader)))

            if self.epochs > 1 and self.verbose >= 10:
                print("Epoch {}/{} complete".format(epoch+1, self.epochs))


    def print_result(self, dataset, indices, to_file=True, filename="temp"):
        """Encodes and decodes n random images from a dataset

        params:
            dataset (object): A torch.utils.data.Dataset object containing the dataset to encode
                              and decode randomly images from
            indices (list): List of indices to take images from. Used for reproducability.
            to_file (bool): Whether to save the results to a file as png. If set to false, will
                            open a window with gtk instead.
            filename (string): Name of the file where the image is stored. Will be ignored if
                               to_file is set to False.
        """
        ims = []
        for i in indices:
            img, _ = dataset[i]
            ims.append(img)
        ims = torch.stack(ims).to(self.device)
        output = self.model(ims)

        combined = torch.cat((ims, output))
        grid = torchvision.utils.make_grid(combined, len(indices))

        if to_file:
            os.makedirs("results", exist_ok=True)
            torchvision.utils.save_image(grid, "./results/{}.png".format(filename))
        else:
            plt.imshow(grid.cpu().detach().numpy().transpose(1, 2, 0))
            plt.show()


    def save(self, filename='autoenc'):
        """Saves the models parameters in the folder model_data"""
        os.makedirs("model_data", exist_ok=True)
        torch.save(self.model.state_dict(), "model_data/{}".format(filename))


    def load(self, filename='autoenc'):
        """Loads the models parameters from the folder model_data"""
        assert os.path.isdir("model_data"), "Cannot load model parameters, no parameters saved"
        self.model.load_state_dict(torch.load("model_data/{}".format(filename)))
