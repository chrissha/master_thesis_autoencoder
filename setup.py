from setuptools import setup, find_packages

setup(
    name="autoenc",
    version="1.0",
    packages=find_packages(),
    scripts=["vae.py"],
    install_requires=["torch", "torchvision", "matplotlib", "numpy"],
)
