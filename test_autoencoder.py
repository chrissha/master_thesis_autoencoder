import torch
from torch import nn
from torchvision import datasets, transforms

from autoencoder import Agent

"""A shallow autoencoder consisting of only one layer"""
class FcShallow(nn.Module):
    def __init__(self, input_size, latent_size):
        super(FcShallow, self).__init__()

        self.input_size = input_size
        self.encoder = nn.Linear(input_size, latent_size)
        self.decoder = nn.Linear(latent_size, input_size)


    def forward(self, input):
        x = input.view(-1, 1, self.input_size)
        x = torch.relu(self.encoder(x))
        x = torch.sigmoid(self.decoder(x))
        x = x.view(input.shape)
        return x

    
if __name__ == "__main__":
    transform = transforms.Compose([
        transforms.ToTensor()
    ])

    # Load dataset
    train = datasets.MNIST("./datasets", train=True, transform=transform, download=True)
    test = datasets.MNIST("./datasets", train=False, transform=transform, download=True)
    mnist_indices = torch.randint(0, len(test), (10,))
    
    # Test autoencoder
    autoenc = FcShallow(28*28, 32)
    agent = Agent(autoenc, epochs=5, batch_size=64, loss='MSE', verbose=10)
    agent.train(train)
    agent.print_result(test, mnist_indices, to_file=True, filename='test_autoenc')

    # Test save and load
    agent.save('autoenc')
    del agent
    agent = Agent(autoenc, epochs=1, batch_size=64, loss='MSE', verbose=20)
    agent.load('autoenc')
    
    agent.print_result(test, mnist_indices, to_file=False)
