"""This file is used to test the vae.py file"""
import torch
import torchvision.datasets as datasets
import torchvision.transforms as transforms
from torch import nn
from vae import VaeAgent, BaseVae

class FcVae(BaseVae):
    """Example fully-connected vae network for MNIST"""
    def __init__(self, input_size, latent_size):
        super(FcVae, self).__init__()

        self.latent_size = latent_size
        self.fc1 = nn.Linear(input_size, 400)
        self.fc2_m = nn.Linear(400, latent_size)
        self.fc2_s = nn.Linear(400, latent_size)
        self.fc3 = nn.Linear(latent_size, 400)
        self.fc4 = nn.Linear(400, input_size)


    def encoder(self, x):
        x = x.flatten(start_dim=1)
        x = torch.relu(self.fc1(x))
        mean = self.fc2_m(x)
        std = self.fc2_s(x)
        return mean, std


    def decoder(self, z):
        z = torch.relu(self.fc3(z))
        z = torch.sigmoid(self.fc4(z))
        return z


"""Example cnn vae network for MNIST"""
class CnnVae(BaseVae):
    def __init__(self, input_size, latent_size):
        super(CnnVae, self).__init__()

        self.latent_size = latent_size
        self.flatten_size = 32 * 4 * 4

        self.cnn1 = nn.Conv2d(1, 32, kernel_size=4, stride=2, padding=1)
        self.cnn2 = nn.Conv2d(32, 32, kernel_size=4, stride=2, padding=1)
        self.cnn3 = nn.Conv2d(32, 32, kernel_size=3, stride=2, padding=1)
        self.fc_mean = nn.Linear(self.flatten_size, latent_size)
        self.fc_std = nn.Linear(self.flatten_size, latent_size)

        self.fc_z = nn.Linear(latent_size, self.flatten_size)
        self.cnnt1 = nn.ConvTranspose2d(32, 32, kernel_size=3, stride=2, padding=1)
        self.cnnt2 = nn.ConvTranspose2d(32, 32, kernel_size=4, stride=2, padding=1)
        self.cnnt3 = nn.ConvTranspose2d(32, 1, kernel_size=4, stride=2, padding=1)
        self.fc_final = nn.Linear(28*28, 28*28)


    def encoder(self, x):
        x = torch.relu(self.cnn1(x))
        x = torch.relu(self.cnn2(x))
        x = torch.relu(self.cnn3(x))
        x = x.view(-1, self.flatten_size)
        mean = self.fc_mean(x)
        std = self.fc_std(x)
        return mean, std


    def decoder(self, z):
        z = self.fc_z(z).view(-1, 32, 4, 4)
        z = torch.relu(self.cnnt1(z))
        z = torch.relu(self.cnnt2(z))
        z = torch.relu(self.cnnt3(z))
        z = torch.sigmoid(self.fc_final(z.view(-1, 28*28))).view(-1, 1, 28, 28)
        return z


if __name__ == "__main__":
    seed = 0
    torch.manual_seed(seed)    

    # Load dataset
    transform = transforms.Compose([
        transforms.ToTensor()
    ])
    train = datasets.MNIST("./datasets", train=True, transform=transform, download=True)
    test = datasets.MNIST("./datasets", train=False, transform=transform, download=True)
    indicies = torch.randint(0, len(test), (10,))

    # Initialize model
    vae_fc = FcVae(28*28, 32)
    vae_cnn = CnnVae(28*28, 32)
    fc_mnist_agent = VaeAgent(vae_fc, epochs=10, batch_size=256, loss='MSE', seed=seed)
    cnn_mnist_agent = VaeAgent(vae_cnn, epochs=10, batch_size=256, loss='MSE', seed=seed)

    fc_mnist_agent.train(train)
    cnn_mnist_agent.train(train)

    fc_mnist_agent.plot_loss(to_file=True, filename='fc_loss')
    cnn_mnist_agent.plot_loss(to_file=True, filename='cnn_loss')

    fc_mnist_agent.save('vae_fc')
    cnn_mnist_agent.save('vae_cnn')

    del fc_mnist_agent
    del cnn_mnist_agent

    fc_mnist_agent = VaeAgent(vae_fc, epochs=10, batch_size=256, loss='MSE', seed=seed)
    cnn_mnist_agent = VaeAgent(vae_cnn, epochs=10, batch_size=256, loss='MSE', seed=seed)

    fc_mnist_agent.load('vae_fc')
    cnn_mnist_agent.load('vae_cnn')

    fc_mnist_agent.print_result(test, indicies, to_file=True, filename='temp_vae')
    cnn_mnist_agent.print_result(test, indicies, to_file=False)