import timeit
import torch
from torch import nn
from torchvision import transforms, datasets

from autoencoder import Agent
from vae import VaeAgent, BaseVae

"""A shallow autoencoder consisting of only one layer"""
class FcShallow(nn.Module):
    def __init__(self, input_size, latent_size):
        super(FcShallow, self).__init__()

        self.input_size = input_size
        self.encoder = nn.Linear(input_size, latent_size)
        self.decoder = nn.Linear(latent_size, input_size)


    def forward(self, input):
        x = input.view(-1, 1, self.input_size)
        x = torch.relu(self.encoder(x))
        x = torch.sigmoid(self.decoder(x))
        x = x.view(input.shape)
        return x

"""A deeper autoencoder consisting of 3 hidden layers"""
class FcDeep(nn.Module):
    def __init__(self, input_size, latent_size):
        super(FcDeep, self).__init__()

        self.input_size = input_size

        # Encoder
        self.fc1 = nn.Linear(input_size, 400)
        self.fc2 = nn.Linear(400, 250)
        self.fc3 = nn.Linear(250, 100)
        self.fc4 = nn.Linear(100, latent_size)

        # Decoder
        self.fc5 = nn.Linear(latent_size, 100)
        self.fc6 = nn.Linear(100, 250)
        self.fc7 = nn.Linear(250, 400)
        self.fc8 = nn.Linear(400, input_size)


    def forward(self, input):
        x = input.view(-1, 1, self.input_size)
        x = torch.relu(self.fc1(x))
        x = torch.relu(self.fc2(x))
        x = torch.relu(self.fc3(x))
        x = torch.relu(self.fc4(x))

        x = torch.relu(self.fc5(x))
        x = torch.relu(self.fc6(x))
        x = torch.relu(self.fc7(x))
        x = torch.sigmoid(self.fc8(x))
        x = x.view(input.shape)
        return x


"""CNN VAE used to run experiments of MNIST dataset"""
class CnnVaeMNIST(BaseVae):
    def __init__(self, input_size, latent_size):
        super(CnnVaeMNIST, self).__init__()

        self.latent_size = latent_size
        self.flatten_size = 128 * 4 * 4
                                                                           # 1x28x28
        self.cnn1 = nn.Conv2d(1, 32, kernel_size=4, stride=2, padding=1)   # 32x14x14
        self.cnn2 = nn.Conv2d(32, 64, kernel_size=4, stride=2, padding=1)  # 64x7x7
        self.cnn3 = nn.Conv2d(64, 128, kernel_size=3, stride=2, padding=1) # 128x4x4
        self.fc_mean = nn.Linear(self.flatten_size, latent_size)           # latent_size
        self.fc_std = nn.Linear(self.flatten_size, latent_size)            # latent_size

        self.fc_z = nn.Linear(latent_size, self.flatten_size)                        # 128x4x4
        self.cnnt1 = nn.ConvTranspose2d(128, 64, kernel_size=3, stride=2, padding=1) # 64x7x7
        self.cnnt2 = nn.ConvTranspose2d(64, 32, kernel_size=4, stride=2, padding=1)  # 32x14x14
        self.cnnt3 = nn.ConvTranspose2d(32, 1, kernel_size=4, stride=2, padding=1)   # 1x28x28
        self.fc_final = nn.Linear(28*28, 28*28)                                      # 1x28x28


    def encoder(self, x):
        x = torch.relu(self.cnn1(x))
        x = torch.relu(self.cnn2(x))
        x = torch.relu(self.cnn3(x))
        x = x.view(-1, self.flatten_size)
        mean = self.fc_mean(x)
        std = self.fc_std(x)
        return mean, std


    def decoder(self, x):
        x = self.fc_z(x).view(-1, 128, 4, 4)
        x = torch.relu(self.cnnt1(x))
        x = torch.relu(self.cnnt2(x))
        x = torch.relu(self.cnnt3(x))
        x = torch.sigmoid(self.fc_final(x.view(-1, 28*28))).view(-1, 1, 28, 28)
        return x


"""CNN VAE used to run experiments of CIFAR10 dataset"""
class CnnVaeCIFAR10(BaseVae):
    def __init__(self, input_size, latent_size):
        super(CnnVaeCIFAR10, self).__init__()

        self.latent_size = latent_size
        self.flatten_size = 128 * 4 * 4
                                                                           # 3x32x32
        self.cnn1 = nn.Conv2d(3, 32, kernel_size=4, stride=2, padding=1)   # 32x16x16
        self.cnn2 = nn.Conv2d(32, 64, kernel_size=4, stride=2, padding=1)  # 64x8x8
        self.cnn3 = nn.Conv2d(64, 128, kernel_size=4, stride=2, padding=1) # 128x4x4
        self.fc_mean = nn.Linear(self.flatten_size, latent_size)           # 32
        self.fc_std = nn.Linear(self.flatten_size, latent_size)            # 32

        self.fc_z = nn.Linear(latent_size, self.flatten_size)                        # 128x4x4
        self.cnnt1 = nn.ConvTranspose2d(128, 64, kernel_size=4, stride=2, padding=1) # 64x8x8
        self.cnnt2 = nn.ConvTranspose2d(64, 32, kernel_size=4, stride=2, padding=1)  # 32x16x16
        self.cnnt3 = nn.ConvTranspose2d(32, 3, kernel_size=4, stride=2, padding=1)   # 3x32x32
        self.fc_final = nn.Linear(32*32*3, 32*32*3)                                  # 3x32x32


    def encoder(self, x):
        x = torch.relu(self.cnn1(x))
        x = torch.relu(self.cnn2(x))
        x = torch.relu(self.cnn3(x))
        x = x.view(-1, self.flatten_size)
        mean = self.fc_mean(x)
        std = self.fc_std(x)
        return mean, std


    def decoder(self, x):
        x = self.fc_z(x).view(-1, 128, 4, 4)
        x = torch.relu(self.cnnt1(x))
        x = torch.relu(self.cnnt2(x))
        x = torch.relu(self.cnnt3(x))
        x = torch.sigmoid(self.fc_final(x.view(-1, 32*32*3))).view(-1, 3, 32, 32)
        return x


if __name__ == "__main__":
    mnist_input = 28*28
    cifar_input = 32*32*3

    torch.manual_seed(0)
    transform = transforms.Compose([
        transforms.ToTensor()
    ])

    # Load dataset
    mnist_train = datasets.MNIST("./datasets", train=True, transform=transform, download=True)
    mnist_test = datasets.MNIST("./datasets", train=False, transform=transform, download=True)
    cifar_train = datasets.CIFAR10("./datasets", train=True, transform=transform, download=True)
    cifar_test = datasets.CIFAR10("./datasets", train=False, transform=transform, download=True)

    mnist_indices = torch.randint(0, len(mnist_test), (10,))
    cifar_indices = torch.randint(0, len(cifar_test), (10,))


    # Test latent-vector sizes
    autoenc = FcShallow(mnist_input, 16)
    agent = Agent(autoenc, epochs=1, batch_size=64)
    start = timeit.default_timer()
    agent.train(mnist_train)
    stop = timeit.default_timer()
    agent.print_result(mnist_test, mnist_indices, filename="fcshallow16_{:.2f}".format(stop - start))
    
    
    autoenc = FcShallow(mnist_input, 32)
    agent = Agent(autoenc, epochs=1, batch_size=64)
    start = timeit.default_timer()
    agent.train(mnist_train)
    stop = timeit.default_timer()
    agent.print_result(mnist_test, mnist_indices, filename="fcshallow32_{:.2f}".format(stop - start))
    
    autoenc = FcShallow(mnist_input, 64)
    agent = Agent(autoenc, epochs=1, batch_size=64)
    start = timeit.default_timer()
    agent.train(mnist_train)
    stop = timeit.default_timer()
    agent.print_result(mnist_test, mnist_indices, filename="fcshallow64_{:.2f}".format(stop - start))


    # Test fc deep network
    autoenc = FcDeep(mnist_input, 32)
    agent = Agent(autoenc, epochs=1, batch_size=64)
    start = timeit.default_timer()
    agent.train(mnist_train)
    stop = timeit.default_timer()
    agent.print_result(mnist_test, mnist_indices, filename="fcdeep1epoch32_{:.2f}".format(stop - start))

    autoenc = FcDeep(mnist_input, 32)
    agent = Agent(autoenc, epochs=10, batch_size=64)
    start = timeit.default_timer()
    agent.train(mnist_train)
    stop = timeit.default_timer()
    agent.print_result(mnist_test, mnist_indices, filename="fcdeep10epoch32_{:.2f}".format(stop - start))

    
    # Test CIFAR10 dataset on deep network
    autoenc = FcDeep(cifar_input, 32)
    agent = Agent(autoenc, epochs=10, batch_size=64)
    start = timeit.default_timer()
    agent.train(cifar_train)
    stop = timeit.default_timer()
    agent.print_result(cifar_test, cifar_indices, filename="fcdeepCIFAR32_{:.2f}".format(stop - start))


    # Test VAE on MNIST and CIFAR10
    autoenc = CnnVaeMNIST(mnist_input, 32)
    agent = VaeAgent(autoenc, epochs=10, batch_size=64)
    start = timeit.default_timer()
    agent.train(mnist_train)
    stop = timeit.default_timer()
    agent.print_result(mnist_test, mnist_indices, filename="vaecnnMNIST32_{:.2f}".format(stop - start))

    autoenc = CnnVaeCIFAR10(cifar_input, 32)
    agent = VaeAgent(autoenc, epochs=10, batch_size=64)
    start = timeit.default_timer()
    agent.train(cifar_train)
    stop = timeit.default_timer()
    agent.print_result(cifar_test, cifar_indices, filename="vaecnnCIFAR32_{:.2f}".format(stop - start))

    autoenc = CnnVaeCIFAR10(cifar_input, 64)
    agent = VaeAgent(autoenc, epochs=10, batch_size=64)
    start = timeit.default_timer()
    agent.train(cifar_train)
    stop = timeit.default_timer()
    agent.print_result(cifar_test, cifar_indices, filename="vaecnnCIFAR64_{:.2f}".format(stop - start))
    