"""This file implements VAE"""
import os
import matplotlib.pyplot as plt
import torch
import torchvision
from torch import nn


class BaseVae(nn.Module):
    """Base class for Vae networks

    This is the base class that every vae network should derive from. By deriving from this class,
    one only has to implement encoder() and decoder() methods. The rest is handled by this base
    class.
    """
    def encoder(self, x):
        """Encodes the image to a mean and std vector

        Applies the encoder part of the autoencoder to the input image and returns the mean and std
        vectors. Must be of same size.

        params:
            x (torch.tensor): The input image to encode.
        returns:
            (torch.tensor): The mean vector
            (torch.tensor): The std vector
        """
        raise NotImplementedError


    def decoder(self, z):
        """Decodes the latent-vector z to an image.

        Applies the decoder part to the latent-vector z and returns the reconstructed image. If the
        network is a fully-connected network, the decoder does not need to reshape it.

        params:
            z (torch.tensor): The latent-vector
        returns:
            (torch.tensor): The reconstructed image.
        """
        raise NotImplementedError


    def forward(self, x):
        """Encodes and decodes an image.

        Forward will perform encoding and decoding of an image and return tensors necessary to
        calculate the loss.

        params:
            x (torch.tensor): The input image to process
        returns:
            (torch.tensor): The reconstructed image
            (torch.tensor): A sample from a normal distribution used to calculate the KL loss.
            (torch.tensor): A standard normal distribution used for reparametrization trick.
        """
        shape = x.shape # Reshape for fc networks
        mean, std = self.encoder(x)
        z, kl_sample, eps = self.calculate_z(mean, std)
        output = self.decoder(z).reshape(shape)
        return output, kl_sample, eps

    
    def calculate_z(self, mean, std):
        """Calculates the z vector given mean and std vectors
        
        params: 
            mean (torch.tensor): The mean vector
            std (torch.tensor): The std vector
        returns:
            (torch.tensor): The latent-vector z
            (torch.tensor): A sample from a normal distribution used to calculate the KL loss.
            (torch.tensor): A standard normal distribution used for reparametrization trick.
        """
        kl_sample = torch.normal(mean, std).to(self.device)
        eps = torch.normal(mean=0., std=1., size=kl_sample.size()).to(self.device)
        z = mean + std * eps
        return z, kl_sample, eps


    def encode_decode(self, x):
        """Encodes and decodes the given input.

        Does the same as self.forward(), but only returns the reconstructed image.
        
        params:
            x (torch.tensor): The input image to encode and decode
        returns:
            (torch.tensor): The reconstructed image.
        """
        output, _, _ = self.forward(x)
        return output


    def get_latent_vector(self, x):
        """Returns the latent-vector z for a given image"""
        z, _, _ = self.calculate_z(*self.encoder(x))
        return z


class VaeAgent():   
    """Agent to handle vae learning

    This class handles everything from hyperparameters, the training of the vae network. Saving and
    loading vae and printing results either with a display or to an image file.
    """
    def __init__(self, vae, epochs=1, batch_size=256, lr=1e-3, loss='MSE', seed=None, verbose=10):
        """Initializes the agent that manages the vae

        params:
            vae (object): The vae to use. The input to this network is defined by the dataset used
                          in self.train(). Needs to derive from BaseVae class.
            epochs (int): How many epochs to train for
            batch_size (int): The batch size used for training
            lr (float): The learning rate used for training
            lambd (float): The L2 regularization parameter for more sparse weights.
            loss (string): The loss function to use for difference between input and the
                           reconstructed image. Can be 'BCE' or 'MSE'.
            seed (int): The seed to use for random numbers, None for random seed
            verbose (int): 0 prints nothing. 10 prints every epoch. 20 prints every episode.
        """
        # Hyperparameters
        self.epochs = epochs
        self.batch_size = batch_size
        self.learning_rate = lr
        self.verbose = verbose
        self.losses = []

        if seed is not None:
            torch.manual_seed(seed)

        self.device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
        
        # Initialize model
        self.model = vae.to(self.device)
        self.model.device = self.device

        # Optimizer and loss function
        self.optimizer = torch.optim.Adam(self.model.parameters(), lr=self.learning_rate) 
        self.diff_loss = nn.BCELoss() if loss == 'BCE' else nn.MSELoss()
        self.kld_loss = nn.KLDivLoss(reduction='batchmean')


    def train(self, dataset):
        """Trains the VAE using the given dataset.

        params:
            dataset (object): A torch.utils.data.Dataset object containing the dataset to train 
                              the autoencoder on. Remember to add a transform with ToTensor()
                              when creating the dataset.
        """
        dataloader = torch.utils.data.DataLoader(dataset, batch_size=self.batch_size, shuffle=True)

        for epoch in range(self.epochs):
            epoch_losses = []
            
            for i, batch in enumerate(dataloader):
                if isinstance(batch, list):
                    batch, _ = batch

                # Make 'prediciton' and calculate loss
                batch = batch.to(self.device)
                predictions, kl_sample, eps = self.model(batch)
                loss = self.diff_loss(predictions, batch) - self.kld_loss(kl_sample, eps)
                epoch_losses.append(loss.detach())
                
                # Backprop
                self.optimizer.zero_grad()
                loss.backward()
                self.optimizer.step()

                if self.verbose >= 20:
                    print("Batch {}/{} complete".format(i+1, len(dataloader)))

            self.losses.append(torch.stack(epoch_losses).mean())
            if self.epochs > 1 and self.verbose >= 10:
                print("Epoch {}/{} complete".format(epoch+1, self.epochs))


    def print_result(self, dataset, indicies, to_file=True, filename="temp"):
        """Encodes and decodes n random images from a dataset
        
        params:
            dataset (object): A torch.utils.data.Dataset object containing the dataset to encode
                              and decode randomly images from
            n (int): How many random images to encode and decode.
            to_file (bool): Whether to save the results to a file as png. If set to false, will
                            open a window with gtk instead.
            filename (string): Name of the file where the image is stored. Will be ignored if
                               to_file is set to False.
        """
        ims = []
        for i in indicies:
            im, _ = dataset[i]
            ims.append(im)
        ims = torch.stack(ims).to(self.device)
        output = self.model.encode_decode(ims)

        combined = torch.cat((ims, output))
        grid = torchvision.utils.make_grid(combined, len(indicies))

        if to_file:
            os.makedirs("results", exist_ok=True)
            torchvision.utils.save_image(grid, "./results/{}.png".format(filename))
        else:
            plt.figure()
            plt.imshow(grid.cpu().detach().numpy().transpose(1, 2, 0))
            plt.show()

    
    def plot_loss(self, to_file=False, filename="loss"):
        """Plots the losses for each epoch and renders it or saves it to file"""
        if len(self.losses) == 0:
            print("Cannot plot losses, training has not been done performed yet")
            return
        assert len(self.losses) == self.epochs
        x = list(range(self.epochs))
        y = [t.numpy() for t in self.losses]

        plt.figure()
        plt.plot(x, y)

        for i, v in enumerate(y):
            plt.text(i, v, "{:.4f}".format(v))

        if to_file:
            plt.savefig("results/{}.png".format(filename))
        else:
            plt.show()


    def save(self, filename='vae'):
        """Saves the models parameters in the folder model_data"""
        os.makedirs("model_data", exist_ok=True)
        torch.save(self.model.state_dict(), "model_data/{}".format(filename))


    def load(self, filename='vae'):
        """Loads the models parameters from the folder model_data"""
        assert os.path.isdir("model_data"), "Cannot load model parameters, no parameters saved"
        self.model.load_state_dict(torch.load("model_data/{}".format(filename)))